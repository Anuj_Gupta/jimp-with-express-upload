const Jimp = require('jimp');
const express = require('express');
const fileUpload = require('express-fileupload');
const aws = require('aws-sdk');
let s3 = new aws.S3({
    signatureVersion: 'v4',
    accessKeyId: "AKIAIQ4Z2D5L27XXBN5A",
    secretAccessKey: "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",
});
const app = express();

app.use(fileUpload({
    limits: { fileSize: 25 * 1024 * 1024 },
    parseNested: true,
    preserveExtension: true
}));



// listen on app
app.listen(5000, () => {
    console.log(`server started at http://localhost:5000`);
}).setTimeout(800000);


app.post('/upload', function (req, res) {
    let imagesPromises = [];
    req.files && req.files.images.forEach(element => {
        imagesPromises.push(applyWaterMark(element.data));
    });

    return Promise.all(imagesPromises)
        .then((uploadImg) => {
            return res.json({ statas: true, uploadImg });
        })
        .catch((err) => {
            console.log(err);
            return res.json({ statas: false });
        })
});

let applyWaterMark = (filePath) => {
    var proimage = Jimp.read(filePath);
    var icon = Jimp.read("https://www.floorindex.com/wp-content/uploads/2019/05/FloorIndexLogo.png");

    return Promise.all([proimage, icon])
        .then(images => {
            return images[0].composite(images[1], 0, 0).getBufferAsync(Jimp.MIME_PNG)
        })
        .then((bufferImg) => {
            let fileName = `this-sample/${new Date().getTime()}.jpg`;
            return uploadImgS3(fileName, bufferImg);
        })
        .then((awsImgPath) => {
            return Promise.resolve(awsImgPath);
        })
        .catch((err) => {
            console.log(err);
        })
}


let uploadImgS3 = async (key, data) => {
    let base64data = new Buffer(data, 'binary');
    let filePath = `https://floor-index.s3.amazonaws.com/${key}`;
    
    let response = await s3.putObject({
        Key: key,
        Bucket: "floor-index",
        Body: base64data,
        ContentType: "image/png", 
    }).promise();
    return filePath;
}
